﻿using System;

namespace ReadConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // Type something and press enter
            Console.WriteLine("Escreve aguma coisa:");

            // Create a string variable and get user input from the keyboard and store it in the variable
            string something = Console.ReadLine();

            // Print the value of the variable (something), which will display the input value
            Console.WriteLine("Escreveste: " + something);
        }
    }
}
