﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AprenderBasicos
{
    class Colecoes
    {

        public void printar()
        {

            // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/collections
            Console.WriteLine("-----------------------------");
            Console.WriteLine("-----------------------------");
            Console.WriteLine("-----------------------------");
            Console.WriteLine("Coleções");

            Console.WriteLine("--- Coleção Animais");
            var animais = new List<string>();
            animais.Add("Cão");
            animais.Add("Ninica");
            animais.Add("Formiga");

            foreach (var animal in animais)
            {
                Console.WriteLine(animal);
            }

            Console.WriteLine("--- Coleção Frutas");
            var frutas = new List<string> { "Maça", "Pêra", "Banana", "Abacaxi", "Pinhões" };
            frutas.Remove("Banana"); // Eleminia e Reorganiza as posições
            for (var i = 0; i < frutas.Count; i++)
            {
                Console.WriteLine(frutas.IndexOf(frutas[i]).ToString() + " - " + frutas[i]);
            }

        }
    }
}
