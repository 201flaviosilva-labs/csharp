﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AprenderBasicos
{
    class Arrays
    {
        public void printar()
        {

            // Matriz: https://docs.microsoft.com/pt-br/dotnet/csharp/tour-of-csharp/features
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Arrays / Vetor");

            int[] array = new int[5]; // Criar um array e determinar 5 posições
            int[] array2 = new int[5] { 10, 46, 34, 20, 40 }; // Criar um array e determinar 5 posições com conteudo
            array[0] = 1;
            Console.WriteLine("Posição 0: " + array[0]);
            Console.WriteLine("------ Array1: ");

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = i + 1;
                Console.WriteLine("Posição " + i + ": " + array[i]);
            }

            Console.WriteLine("------ Array2: ");
            for (int i = 0; i < array2.Length; i++)
            {
                Console.WriteLine("Posição " + i + ": " + array2[i]);
            }

            Console.WriteLine("-------------------------------------");
            Console.WriteLine("Matris / Array Bidimencional");
            string[,] matrisString = new string[3, 5];
            matrisString[0, 0] = "Zé";
            matrisString[1, 2] = "Povinho";

            Console.WriteLine("--- Forma 1: ");
            foreach (string s in matrisString)
            {
                Console.WriteLine("{0} ", s);
            }

            Console.WriteLine("--- Forma 2: ");
            for (int i = 0; i <= matrisString.GetUpperBound(0); i++)
            {
                Console.WriteLine("Linha: " + i);
                for (int j = 0; j <= matrisString.GetUpperBound(0); j++)
                {
                    if (matrisString[i, j] == null) matrisString[i, j] = "Vazio";
                    Console.Write(" \\|/ Coluna: " + j + " -> " + matrisString[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}
