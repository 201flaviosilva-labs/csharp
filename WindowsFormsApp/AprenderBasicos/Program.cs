﻿using System;

namespace AprenderBasicos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Arrays arrays = new Arrays();
            arrays.printar();

            Colecoes colecoes = new Colecoes();
            colecoes.printar();
        }
    }
}
