﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdivinhaNumero
{

    // Global Game Configs
    public static class vars
    {
        public static int numberRandom;

        public static int numberRndMin = 0;
        public static int numberRndMax = 100;

        public static int CreateRandomNumber()
        {
            // Create a Random number
            int rnd = new Random(DateTime.Now.Millisecond).Next(numberRndMin, numberRndMax);
            return rnd;
        }
    }
}
