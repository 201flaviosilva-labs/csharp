﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdivinhaNumero
{
    public partial class frmStart : Form
    {
        public frmStart()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            // Exit Game
            Application.Exit();

        }

        private void btnJogar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmGame gameForm = new frmGame();
            gameForm.ShowDialog();
            this.Show();
        }

        private void btnConfigs_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmSetup setup = new frmSetup();
            setup.ShowDialog();
            this.Show();
        }
    }
}
