﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdivinhaNumero
{
    public partial class frmSetup : Form
    {
        public frmSetup()
        {
            InitializeComponent();
        }
        private void frmSetup_Load(object sender, EventArgs e)
        {
            numUpDownMin.Value = vars.numberRndMin;
            numUpDownMax.Value = vars.numberRndMax;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            int minValue = (int)numUpDownMin.Value;
            int maxValue = (int)numUpDownMax.Value;

            if ((minValue + 5) <= maxValue)
            {
                label4.Visible = true;
                label4.Text = "O Valor maximo tem que ser maior que o minimo e como uma diferença de 5";
                return;
            }

            vars.numberRndMin = minValue;
            vars.numberRndMax = maxValue;
            this.Close();
        }
    }
}
