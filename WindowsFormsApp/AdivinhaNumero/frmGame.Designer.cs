﻿
namespace AdivinhaNumero
{
    partial class frmGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGess = new System.Windows.Forms.Button();
            this.lableOutput = new System.Windows.Forms.Label();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnGenNUmber = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGess
            // 
            this.btnGess.Location = new System.Drawing.Point(121, 125);
            this.btnGess.Name = "btnGess";
            this.btnGess.Size = new System.Drawing.Size(75, 23);
            this.btnGess.TabIndex = 0;
            this.btnGess.Text = " Adivinhar";
            this.btnGess.UseVisualStyleBackColor = true;
            this.btnGess.Click += new System.EventHandler(this.btnGess_Click);
            // 
            // lableOutput
            // 
            this.lableOutput.Location = new System.Drawing.Point(0, 40);
            this.lableOutput.Name = "lableOutput";
            this.lableOutput.Size = new System.Drawing.Size(350, 13);
            this.lableOutput.TabIndex = 1;
            this.lableOutput.Text = "Um Número foi escondido entre 0 e 100";
            this.lableOutput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(106, 85);
            this.txtInput.MaxLength = 10;
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(100, 20);
            this.txtInput.TabIndex = 2;
            this.txtInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInput_KeyDown);
            this.txtInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInput_KeyPress);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(12, 242);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Voltar";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnGenNUmber
            // 
            this.btnGenNUmber.Location = new System.Drawing.Point(217, 242);
            this.btnGenNUmber.Name = "btnGenNUmber";
            this.btnGenNUmber.Size = new System.Drawing.Size(108, 23);
            this.btnGenNUmber.TabIndex = 4;
            this.btnGenNUmber.Text = "Gerar Novo Num.";
            this.btnGenNUmber.UseVisualStyleBackColor = true;
            this.btnGenNUmber.Click += new System.EventHandler(this.btnGenNUmber_Click);
            // 
            // frmGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 277);
            this.ControlBox = false;
            this.Controls.Add(this.btnGenNUmber);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.lableOutput);
            this.Controls.Add(this.btnGess);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGame";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jogo";
            this.Load += new System.EventHandler(this.frmGame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGess;
        private System.Windows.Forms.Label lableOutput;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnGenNUmber;
    }
}