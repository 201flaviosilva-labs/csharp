﻿using System;
using System.Windows.Forms;

namespace AdivinhaNumero
{
    public partial class frmGame : Form
    {

        int numTrys = 0;
        public frmGame()
        {
            InitializeComponent();
        }
        private void frmGame_Load(object sender, EventArgs e)
        {

            this.generateRandom();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnGess_Click(object sender, EventArgs e)
        {
            txtInput.Focus();
            string numInput = txtInput.Text;
            // Check if is empty
            if (numInput == "")
            {
                lableOutput.Text = "" + numTrys + " - Tens de escrever algum número";
                return;
            }

            // Check if is a number
            int temp = 0;
            bool isNumber = int.TryParse(numInput, out temp);

            if (!isNumber)
            {
                lableOutput.Text = "" + numTrys + " - Humm! Isso não é um Número :( " + " ಠ╭╮ಠ";
                txtInput.Text = "";
                return;
            }

            // Add trys
            numTrys++;

            // Compare Numbers
            int playerNumber = Int16.Parse(numInput);
            if (playerNumber > vars.numberRndMax) lableOutput.Text = "" + numTrys + " - O teu número é maior que o limite (" + vars.numberRndMax + ")";
            else if (playerNumber < vars.numberRndMin) lableOutput.Text = "" + numTrys + " - O teu número é menor que o limite (" + vars.numberRndMin + ")";
            else if (playerNumber < vars.numberRandom) lableOutput.Text = "" + numTrys + " - O teu número é MENOR que o escondido! Tenta outra vez :)";
            else if (playerNumber > vars.numberRandom) lableOutput.Text = "" + numTrys + " - O teu número é MAIOR que o escondido! Tenta outra vez :)";
            else if (playerNumber == vars.numberRandom) lableOutput.Text = "" + numTrys + " - MUITO BEM CONSEGUISTE ACERTAR (•◡•)";
            else lableOutput.Text = "" + numTrys + " - Algo deu errado, confirma se realmente escreveste apenas números! " + vars.numberRandom;
        }

        private void btnGenNUmber_Click(object sender, EventArgs e)
        {
            this.generateRandom();
        }

        private void generateRandom()
        {
            numTrys = 0;
            lableOutput.Text = "Um Número foi escondido entre 0 e 100";
            vars.numberRandom = vars.CreateRandomNumber();
            Console.WriteLine(vars.numberRandom);
        }

        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            // On click Return try find number
            if (e.KeyCode == Keys.Return)
            {
                btnGess_Click(btnGess, EventArgs.Empty);
            }
        }

        private void txtInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            string avaliableKey = "0123456789";
            if (!avaliableKey.Contains(e.KeyChar.ToString()) && Convert.ToInt16(e.KeyChar) != 8)
            {
                e.Handled = true;
            }
        }
    }
}
